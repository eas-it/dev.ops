# Multiple Origin Repositories

This git repository is connected with both Gitlab and Github servers.

Please run the follow configuration commands before commit/push

```bash
git remote add origin git@gitlab.com:eas-it/dev.ops.git
git remote set-url --add origin git@github.com:mis-t/it.ops.git
```