## Add multiple remote url when pushing origin branch

```
git remote add origin first-repository-url
git remote set-url --add origin second-repository-url
```
