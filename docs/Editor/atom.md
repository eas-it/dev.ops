# Atom

## Shortcut Keys

| Description | Keys |
|--- |--- |
| Make selected text lowercase | Ctrl + K, then Ctrl + L |
| Make selected text uppercase | Ctrl + K, then Ctrl + U |
